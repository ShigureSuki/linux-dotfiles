if [ -z "$_INIT_SH_LOADED" ]; then
	_INIT_SH_LOADED=1
else
	return
fi

case "$-" in
	*i*) ;;
	  *) return;;
esac

# START_TIME=`date +%s.%N`

# user-friendly settings
if [ -d "$HOME/bin" ]; then
	export PATH="$HOME/bin:$PATH"
fi
## make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set base directory as well as shell,
# and choose different style for bash/zsh.
if [ -n "$BASH_VERSION" ]; then
	_SHELL_NOW="bash"
	BASE_DIR=`dirname $BASH_SOURCE`
	_PROMPT_FILE="$BASE_DIR/prompt_default.sh"
else
	_SHELL_NOW="zsh"
	BASE_DIR=$(dirname `readlink -f $0`) # perhaps only work on linux
	_PROMPT_FILE="$BASE_DIR/prompt_fish.sh"
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
	xterm-color|*-256color) _COLOR_PROMPT=yes;;
esac

## uncomment for a colored prompt, if the terminal has the capability
#_FORCE_COLOR_PROMPT=yes

if [ -n "$_FORCE_COLOR_PROMPT" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	_COLOR_PROMPT=yes
    else
	_COLOR_PROMPT=
    fi
fi

if [ "$_COLOR_PROMPT" = yes ]; then
	test -s "$_PROMPT_FILE" && . "$_PROMPT_FILE" || true
else
	test -s "$_PROMPT_FILE" && env _COLOR_PROMPT=no source "$_PROMPT_FILE" || true
fi

unset _PROMPT_FILE _COLOR_PROMPT _FORCE_COLOR_PROMPT

## enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

## colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'


# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Remove duplicate path.
if [ -n "$PATH" ]; then
    old_PATH=$PATH:; PATH=
	while [ -n "$old_PATH" ]; do
        x=${old_PATH%%:*}
        case $PATH: in
           *:"$x":*) ;;
           *) PATH=$PATH:$x;;
        esac
        old_PATH=${old_PATH#*:}
    done
    PATH=${PATH#:}
    unset old_PATH x
fi

export PATH

# security settings
umask 022

# load shell-specific settings
test -s "$BASE_DIR/${_SHELL_NOW}-specific/${_SHELL_NOW}rc" && . "$BASE_DIR/${_SHELL_NOW}-specific/${_SHELL_NOW}rc" || true

# load z.lua
if [ -f /usr/bin/lua ]; then
	eval "$(lua $BASE_DIR/z.lua/z.lua --init ${_SHELL_NOW} enhanced once)"
fi

# load aliases and commmon special function(s)
test -s "$BASE_DIR/alias" && . "$BASE_DIR/alias" || true
## special funcs
if [ -s "$BASE_DIR/ccat.sh" ]; then
	. "$BASE_DIR/ccat.sh"
	alias cat='ccat'
fi

# set astyle option
# export ARTISTIC_STYLE_OPTIONS="$BASE_DIR/../astyle/.astylerc"

# terminal settings 
## bind language to english to defend CJK font not found
CONSOLE_TYPE=$(tty | awk '/^\/dev\/[^\/]+$/ {print "vt"}')

if [ -n "$LANG" ]; then
    case $LANG in
        *.utf8*|*.UTF-8*)
        if [ "$TERM" = "linux" ]; then
            if [ "$CONSOLE_TYPE" = "vt" ]; then
                case $LANG in
                    ja*) LANG=en_US.UTF-8; LANGUAGE=en_US:en ;;
                    ko*) LANG=en_US.UTF-8; LANGUAGE=en_US:en ;;
                    si*) LANG=en_US.UTF-8; LANGUAGE=en_US:en ;;
                    zh*) LANG=en_US.UTF-8; LANGUAGE=en_US:en ;;
                    ar*) LANG=en_US.UTF-8; LANGUAGE=en_US:en ;;
                    fa*) LANG=en_US.UTF-8; LANGUAGE=en_US:en ;;
                    he*) LANG=en_US.UTF-8; LANGUAGE=en_US:en ;;
                    en_IN*) ;;
                    *_IN*) LANG=en_US.UTF-8 ;;
                esac
            fi
        fi
        ;;
        *)
        if [ "$TERM" = "linux" ]; then
            if [ "$CONSOLE_TYPE" = "vt" ]; then
                case $LANG in
                    ja*) LANG=en_US.UTF-8; LANGUAGE=en_US:en ;;
                    ko*) LANG=en_US.UTF-8; LANGUAGE=en_US:en ;;
                    si*) LANG=en_US.UTF-8; LANGUAGE=en_US:en ;;
                    zh*) LANG=en_US.UTF-8; LANGUAGE=en_US:en ;;
                    ar*) LANG=en_US.UTF-8; LANGUAGE=en_US:en ;;
                    fa*) LANG=en_US.UTF-8; LANGUAGE=en_US:en ;;
                    he*) LANG=en_US.UTF-8; LANGUAGE=en_US:en ;;
                    en_IN*) ;;
                    *_IN*) LANG=en_US.UTF-8 ;;
                esac
            fi
        fi
        ;;
    esac
fi
unset CONSOLE_TYPE

# XDG-specific, explicitly define some variables
if [ -z "$XDG_CONFIG_HOME" ]; then
	export XDG_CONFIG_HOME="$HOME/.config"
fi
if [ -z "$XDG_DATA_HOME" ]; then
	export XDG_DATA_HOME="$HOME/.local/share"
fi

unset BASE_DIR
unset _SHELL_NOW
# END_TIME=`date +%s.%N`
# echo "init.sh load time: $(echo "$END_TIME-$START_TIME" | bc -l)"
# unset START_TIME
# unset END_TIME
